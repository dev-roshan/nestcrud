import React ,{ Component } from 'react';
import axios from 'axios';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import {  Link } from 'react-router-dom';


export default class CandidatesList extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoaded: true,
            items: [],
            pageItems: [],
            page: 1,
            pageSize: 10
        }
      
    }

    componentWillMount() {
      debugger;
        const pageSz = this.state.pageSize;
        axios.get('http://localhost:3001/candidates')
        .then(response =>{
            this.setState({
                items: response.data,
                pageItems: response.data.slice(0, pageSz)
              });
        })
        .catch(error => {
            console.log(error)
        });
    }

    render(){
        if(this.state.items.length !== 0){
        const { isLoaded, pageItems, items, page, pageSize } = this.state;
        const startIndex=(page -1 )* pageSize;
        const endIndex=page * pageSize;
        const pgntItems= items.slice(startIndex,endIndex);
        // const pages = Math.ceil(items.length / page);
        const isLastPage = pgntItems.length !== pageSize || endIndex === items.length;
        if (!isLoaded) {
          return <div>Loading...</div>;
        } else {
          return (
            <div class="container">
                <div class="row">
                <div class="col">
                  <div class="panel panel-primary">
                    <div class="panel-heading">
                      <div class="pull-right">
                     
                      </div>
                    </div>
                    <table class="table table-hover table-bordered" id="task-table">
                      <thead class="thead-dark">
                        <tr>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Nationality</th>
                        <th>DOB</th>
                        <th>Communication prefered</th>
                        <th>Detail</th>
                        </tr>
                      </thead>
                      <tbody>
                      {pgntItems.map(item => {
                            // converting iso date string to date
                            const date = new Date(item.dob);
                            const dob=date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate();
                            return (
                              <tr key={item.id}>
                                <td>{item.name}</td>
                                <td>{item.gender==="0"?'Male':(item.gender==="1"?'Female':'Other')}</td>
                                <td>{item.email}</td>
                                <td>{item.phone}</td>
                                <td>{item.nationality}</td>
                                <td>{dob}</td>
                                <td>{item.prefered_mode==="0"?'Email':(item.prefered_mode==="1"?'Phone':'None')}</td>
                      
                                <td><Link to={'/detail/'+item.id} className="nav-link">click</Link></td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                    <nav>
                      <Pagination>
                        <PaginationItem className={this.state.page === 1 ? 'd-none' : ''} onClick={() => this.setState({page: page -1})}>
                          <PaginationLink>
                            Back
                          </PaginationLink>
                          </PaginationItem>
                        <PaginationItem className={isLastPage === true ? 'd-none' : ''} onClick={() => this.setState({page: page + 1})} >
                          <PaginationLink next tag="button">
                            Next
                          </PaginationLink>
                        </PaginationItem>
                        <PaginationItem></PaginationItem>
                      </Pagination>
                      </nav>
                  </div>
                </div>
                
              </div>
            </div>
          );
        }
      }
      else{
        return <div>Loading...</div>;

      }
    }
    
}



