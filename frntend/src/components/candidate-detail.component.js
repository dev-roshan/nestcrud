import React ,{ Component } from 'react';
import axios from 'axios';
import {  Link } from 'react-router-dom';


export default class CandidateDetail extends Component {
    constructor(props){
        super(props);
        this.state={
            candidate_id:props.match.params.id,
            candidate_name:'',
            candidate_email:'',
            address:'',
            gender:'0',
            phone:'',
            nationality:null,
            dob:'',
            preferred_mode:'0',
            education:'',
            notFound:true
        }
      
    }
    componentDidMount(){
        axios.get('http://localhost:3001/candidates/'+this.state.candidate_id)
        .then(response =>{
            const date = new Date(response.data[0].dob);
            const dob_custom=date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate();
            this.setState({
                candidate_name:  response.data[0].name,
                candidate_email:response.data[0].email,
                address:response.data[0].address,
                gender:response.data[0].gender,
                phone:response.data[0].phone,
                dob:dob_custom,
                nationality:response.data[0].nationality,
                preferred_mode:response.data[0].prefered_mode,
                education:response.data[0].education,
                notFound:false
            })
            

        
        })
        .catch(error => {
            this.state.notFound=true;
        });
    }
    render(){
        if(this.state.notFound){
            return(<h3>Details not found.</h3>)
        }
        else
        {
        return (
            <div style={{marginTop: 10}}>
                <h3>{this.state.name} Details</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-row">
                        <div className="col-md-4">
                        <label>Name:</label>
                        <input type="text" className="form-control" 
                                value={this.state.candidate_name}
                                readOnly
                        />
                        </div>
                        <div className="col-md-4">
                        <label>Email:</label>
                        <input type="email" className="form-control" 
                                value={this.state.candidate_email}
                                readOnly
                        />
                        </div>
                        <div className="col-md-4">
                            <label>Phone: </label>
                            <input type="email" className="form-control" 
                                value={this.state.phone}
                                readOnly />
                        </div>
                        
                    </div>
                    <div className="form-row" style={{marginTop: 10}}>
                        <div className="col-md-4">
                                <div className="form-row">
                                    <label>Gender: </label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="gender" value="0" checked={this.state.gender === "0"}  readOnly required />
                                    <label className="form-check-label" for="inlineRadio1">Male</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="gender" value="1" checked={this.state.gender === "1"}  readOnly required />
                                    <label className="form-check-label" for="inlineRadio1">Female</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="gender" value="2" checked={this.state.gender === "2"}  readOnly required />
                                    <label className="form-check-label" for="inlineRadio1">Other</label>
                                </div>
                        </div>
                        <div className="col-md-4">
                                    <label>Address: </label>
                                    <input type="text" className="form-control" value={this.state.address} required readOnly />
                        </div>
                        <div className="col-md-4">
                            <label>Nationality: </label>
                            <input type="text" className="form-control" value={this.state.nationality} readOnly />
                        </div>
                    </div>

                    <div className="form-row" style={{marginTop: 10}}>
                            <div className="col-md-4">
                                <div className="form-row">
                                    <label>Date of birth: </label>
                                </div>
                            <input type="text" className="form-control" value={this.state.dob} readOnly />
                            </div> 
                            <div className="col-md-4">
                                <div className="form-row">
                                    <label>Preferred mode of contact: </label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="prefered_mode" value="0" checked={this.state.preferred_mode === "0"}  readOnly />
                                    <label className="form-check-label" for="inlineRadio1">Email</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="prefered_mode" value="1" checked={this.state.preferred_mode === "1"}  readOnly />
                                    <label className="form-check-label" for="inlineRadio1">Phone</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="prefered_mode" value="2" checked={this.state.preferred_mode === "2"}  readOnly />
                                    <label className="form-check-label" for="inlineRadio1">None</label>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <label for="exampleFormControlTextarea1">Education Background</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" value={this.state.education} rows="3" readOnly></textarea>
                            </div>
                        </div>
                    <div className="form-group"  style={{marginTop: 10}}>
                    <Link to='/' className="nav-link">
                        <a href="#" class="btn btn-success btn-lg active" role="button" aria-pressed="true">Back</a>
                    </Link>

                        {/* <input type="submit" value="Add Candidate" className="btn btn-primary" /> */}
                    </div>
                </form>
            </div>
        )
        }
    }
}