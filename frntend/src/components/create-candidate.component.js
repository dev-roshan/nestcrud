import React ,{ Component } from 'react';
import axios from 'axios';

import DatePicker from "react-datepicker";
import PhoneInput from 'react-phone-input-2';
import Select from 'react-select';
import countryList from 'react-select-country-list';

import 'react-phone-input-2/lib/style.css';
import "react-datepicker/dist/react-datepicker.css";


export default class CreateCandidate extends Component {

    constructor(props){
        // calling parents constructor
        super(props);

        this.options = countryList().getData()

        this.onChangeCandidateEmail = this.onChangeCandidateEmail.bind(this);
        this.onChangeCandidateName = this.onChangeCandidateName.bind(this);
        this.onChangeCandidateGender = this.onChangeCandidateGender.bind(this);
        this.onChangeCandidateAddress = this.onChangeCandidateAddress.bind(this);
        this.onChangeCandidatePreferred = this.onChangeCandidatePreferred.bind(this);
        this.onChangeCandidateEducation = this.onChangeCandidateEducation.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state={
            options:this.options,
            candidate_name:'',
            candidate_email:'',
            address:'',
            gender:0,
            phone:'',
            nationality:null,
            dob:'',
            preferred_mode:'',
            education:'',
            phoneerror:false
        }

    }

    onChangeCandidateEmail(e){
        this.setState({
            candidate_email: e.target.value
        })
    }
    onChangeCandidateName(e){
        if(!this.state.nationality){
            document.getElementById('nationality_select').required=true;
        }
        this.setState({
            candidate_name: e.target.value
        })
    }

    onChangeCandidateGender(e){
        this.setState({
            gender: e.target.value
        })
    }
    onChangeCandidateAddress(e){
        this.setState({
            address: e.target.value
        })
    }

    onChangeCandidatePreferred(e){
        this.setState({
            preferred_mode: e.target.value
        })
    }

    onChangeCandidateEducation(e){
        this.setState({
            education: e.target.value
        })
    }

    onSubmit(e){
        e.preventDefault();
        if(!this.state.phoneerror){
            const newCandidate={
                name: this.state.candidate_name,
                gender: parseInt(this.state.gender),
                email: this.state.candidate_email,
                phone: this.state.phone,
                address: this.state.address,
                nationality: this.state.nationality,
                dob: this.state.dob,
                education : this.state.education,
                prefered_mode: parseInt(this.state.preferred_mode)
            };
            axios.post('http://localhost:3001/candidates', newCandidate)
                .then(res=> console.log(res))
                .catch(error => {
                    console.log(error)
                });
    
            this.setState({
                candidate_name: '',
                gender:'',
                candidate_email: '',
                phone:'',
                address:'',
                nationality:'',
                dob:'',
                education:'',
                prefered_mode:''
            })
            this.props.history.push('/');
        }
    }

    nationalityChangeHandler = value => {
        if(value.label){
        document.getElementById('nationality_select').required=false;
        }
        this.setState({
            nationality: value.label
          });
    }

    onChangePhoneNumber = phone =>{
        if(this.state.phone.length<9 || this.state.phone.length>15){
            this.setState({phoneerror:true})
        }
        else{
            this.setState({phoneerror:false})
        }
         this.setState({ phone })
    }

    handleChange = date => {
        this.setState({
          dob: date
        });
      };

    render(){
     
        return (
            <div style={{marginTop: 10}}>
                <h3>Add new Candidate</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-row">
                        <div className="col-md-4">
                        <label>Name:</label>
                        <input type="text" className="form-control" 
                                value={this.state.candidate_name}
                                onChange={this.onChangeCandidateName}
                                required
                        />
                        </div>
                        <div className="col-md-4">
                        <label>Email:</label>
                        <input type="email" className="form-control" 
                                value={this.state.candidate_email}
                                onChange={this.onChangeCandidateEmail}
                                required
                        />
                        </div>
                        <div className="col-md-4">
                            <label>Phone: </label>
                                <PhoneInput
                                inputProps={{
                                    required: true,
                                    id:"phone"
                                  }}
                                value={this.state.phone}
                                onChange={this.onChangePhoneNumber}
                                required="true"
                                />
                                 <div className="invalid-feedback"  style={{display:this.state.phoneerror?'block':'none'}}>
                                    Please enter valid phone number.
                                </div>
                            {/* <input type="text" className="form-control" 
                                placeholder="+9779867306575"
                                pattern=".{10,15}"
                                title="please enter a valid phone number"
                                required
                            /> */}
                        </div>
                        
                    </div>
                    <div className="form-row" style={{marginTop: 10}}>
                        <div className="col-md-4">
                                <div className="form-row">
                                    <label>Gender: </label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="gender" value="0" checked={this.state.gender === "0"} onChange={this.onChangeCandidateGender} required />
                                    <label className="form-check-label" for="inlineRadio1">Male</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="gender" value="1" checked={this.state.gender === "1"} onChange={this.onChangeCandidateGender} required />
                                    <label className="form-check-label" for="inlineRadio1">Female</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="gender" value="2" checked={this.state.gender === "2"} onChange={this.onChangeCandidateGender} required />
                                    <label className="form-check-label" for="inlineRadio1">Other</label>
                                </div>
                        </div>
                        <div className="col-md-4">
                                    <label>Address: </label>
                                    <input type="text" className="form-control" value={this.state.address} required onChange={this.onChangeCandidateAddress} />
                        </div>
                        <div className="col-md-4">
                            <label>Nationality: </label>
                                <Select
                                ref="nationality"
                                options={this.state.options}
                                onChange={this.nationalityChangeHandler}
                                inputId="nationality_select"
                                />
                                
                            {/* <input type="text" className="form-control" required /> */}
                        </div>
                    </div>

                    <div className="form-row" style={{marginTop: 10}}>
                            <div className="col-md-4">
                                <div className="form-row">
                                    <label>Date of birth: </label>
                                </div>
                                    <DatePicker
                                            selected={this.state.dob}
                                            onChange={this.handleChange}
                                            required
                                            className="form-control"
                                            maxDate={new Date()}
                                        /> 
                            </div> 
                            <div className="col-md-4">
                                <div className="form-row">
                                    <label>Preferred mode of contact: </label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="prefered_mode" value="0" checked={this.state.preferred_mode === "0"} onChange={this.onChangeCandidatePreferred} required />
                                    <label className="form-check-label" for="inlineRadio1">Email</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="prefered_mode" value="1" checked={this.state.preferred_mode === "1"} onChange={this.onChangeCandidatePreferred} required />
                                    <label className="form-check-label" for="inlineRadio1">Phone</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="prefered_mode" value="2" checked={this.state.preferred_mode === "2"} onChange={this.onChangeCandidatePreferred} required />
                                    <label className="form-check-label" for="inlineRadio1">None</label>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <label for="exampleFormControlTextarea1">Education Background</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" value={this.state.education} rows="3" onChange={this.onChangeCandidateEducation}></textarea>
                            </div>
                        </div>
                    <div className="form-group"  style={{marginTop: 10}}>
                        <input type="submit" value="Add Candidate" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}