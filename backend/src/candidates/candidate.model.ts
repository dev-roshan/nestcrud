import { Length , IsInt, Min, Max ,IsEmail, IsPhoneNumber ,IsDefined, IsString, IsDateString, isDefined, isInt} from 'class-validator';

export class Candidate {


    id : number

    @IsDefined()
    @IsString()
    @Length(3,25,{message: 'Name must be string between 3 and 25'})
    name:string

    @IsDefined()
    @IsInt()
    @Min(0)
    @Max(2)
    gender:number

    @IsDefined()
    email: string;

    @IsDefined()
    @IsString()
    @Length(10,15,{message: 'phone invalid'})
    phone:number

    @IsDefined()
    @IsString()
    address : string

    @IsString()
    nationality: string

    @IsDateString()
    @IsDefined()
    dob: Date

    education : string

    @IsDefined()
    @IsInt()
    @Min(0)
    @Max(2)
    prefered_mode : number

    
}